"""
https://www.codewars.com/kata/5259b20d6021e9e14c0010d4

Complete the function that accepts a string parameter,
and reverses each word in the string. All spaces in the string should be retained.
"""


def reverse_words(text):
    reversed_list = []

    # We need to reverse words only
    for word in text.split(' '):
        reversed_word = ''
        for char in reversed(word):
            reversed_word += char
        reversed_list.append(reversed_word)

    return ' '.join(reversed_list)
