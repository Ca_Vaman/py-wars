"""
https://www.codewars.com/kata/5831c204a31721e2ae000294/train/python

When provided with a String, capitalize all vowels

For example:
Input : "Hello World!"
Output : "HEllO WOrld!"

Note: Y is not a vowel in this kata.
"""


def swap(entry_string):
    capitalized_vowels_string = entry_string\
        .replace("a", "A")\
        .replace("e", "E")\
        .replace("i", "I")\
        .replace("o", "O")\
        .replace("u", "U")
    return capitalized_vowels_string
