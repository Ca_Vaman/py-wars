import unittest
import seven_kyu
import six_kyu


class TestSixKyu(unittest.TestCase):
    def test_jojo_name(self):
        self.assertTrue(six_kyu.jojo_name.is_jojo("Jonathan Joestar"))
        self.assertFalse(six_kyu.jojo_name.is_jojo("Dio Brando"))


class TestSevenKyu(unittest.TestCase):
    def test_closest_number(self):
        self.assertEqual(seven_kyu.closest_number.closest_multiple_10(54), 50)
        self.assertEqual(seven_kyu.closest_number.closest_multiple_10(55), 60)

    def test_capitalize_vowels(self):
        self.assertEqual("HEllO WOrld!", seven_kyu.capitalize_vowels.swap("Hello World!"))
        self.assertEqual("Try nOt tO fAll IntO thIs trAp!",
                         seven_kyu.capitalize_vowels.swap("Try not to fall into this trap!"))

    def test_suzuki_vegetables(self):
        lst1 = [(2, 'tofu'),
                (2, 'potato'),
                (2, 'cucumber'),
                (2, 'cabbage'),
                (1, 'turnip'),
                (1, 'pepper'),
                (1, 'onion'),
                (1, 'mushroom'),
                (1, 'celery'),
                (1, 'carrot')]
        s1 = 'potato tofu cucumber cabbage turnip pepper onion carrot celery mushroom potato tofu cucumber cabbage'
        self.assertEqual(seven_kyu.suzuki_vegetables.count_vegetables(s1), lst1)
        lst2 = [(15, 'turnip'),
                (15, 'mushroom'),
                (13, 'cabbage'),
                (10, 'carrot'),
                (9, 'potato'),
                (7, 'onion'),
                (6, 'tofu'),
                (6, 'pepper'),
                (5, 'cucumber'),
                (4, 'celery')]
        s2 = (
            'mushroom chopsticks chopsticks turnip mushroom carrot mushroom cabbage mushroom carrot tofu pepper cabbage potato cucumber '
            'mushroom mushroom mushroom potato turnip chopsticks cabbage celery celery turnip pepper chopsticks potato potato onion cabbage cucumber '
            'onion pepper onion cabbage potato tofu carrot cabbage cabbage turnip mushroom cabbage cabbage cucumber cabbage chopsticks turnip pepper '
            'onion pepper onion mushroom turnip carrot carrot tofu onion tofu chopsticks chopsticks chopsticks mushroom cucumber chopsticks carrot '
            'potato cabbage cabbage carrot mushroom chopsticks mushroom celery turnip onion carrot turnip cucumber carrot potato mushroom turnip  '
            'mushroom cabbage tofu turnip turnip turnip mushroom tofu potato pepper turnip potato turnip celery carrot turnip')
        self.assertEqual(seven_kyu.suzuki_vegetables.count_vegetables(s2), (lst2))

    def test_alphabetically_ordered(self):
        self.assertEqual(seven_kyu.alphabetically_ordered.alphabetic('asd'), False)
        self.assertEqual(seven_kyu.alphabetically_ordered.alphabetic('codewars'), False)
        self.assertEqual(seven_kyu.alphabetically_ordered.alphabetic('door'), True)
        self.assertEqual(seven_kyu.alphabetically_ordered.alphabetic('cell'), True)
        self.assertEqual(seven_kyu.alphabetically_ordered.alphabetic('z'), True)
        self.assertEqual(seven_kyu.alphabetically_ordered.alphabetic(''), True)

    def test_reverse_words(self):
        self.assertEqual(seven_kyu.reverse_words.reverse_words('The quick brown fox jumps over the lazy dog.'),
                           'ehT kciuq nworb xof spmuj revo eht yzal .god')
        self.assertEqual(seven_kyu.reverse_words.reverse_words('apple'), 'elppa')
        self.assertEqual(seven_kyu.reverse_words.reverse_words('a b c d'), 'a b c d')
        self.assertEqual(seven_kyu.reverse_words.reverse_words('double  spaced  words'), 'elbuod  decaps  sdrow')

    def test_roman_numbers(self):
        self.assertEqual(21, seven_kyu.convert_to_int('XXI'))
        self.assertEqual(1, seven_kyu.convert_to_int('I'))
        self.assertEqual(4, seven_kyu.convert_to_int('IV'))
        self.assertEqual(2008, seven_kyu.convert_to_int('MMVIII'))
        self.assertEqual(1666, seven_kyu.convert_to_int('MDCLXVI'))






