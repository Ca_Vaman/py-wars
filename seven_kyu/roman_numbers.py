"""1666
https://www.codewars.com/kata/51b6249c4612257ac0000005/

Create a function that takes a Roman numeral as its argument and returns its value as a numeric decimal integer.
You don't need to validate the form of the Roman numeral.

Modern Roman numerals are written by expressing each decimal digit of the number to be encoded separately,
starting with the leftmost digit and skipping any 0s.
So 1990 is rendered "MCMXC" (1000 = M, 900 = CM, 90 = XC) and 2008 is rendered "MMVIII" (2000 = MM, 8 = VIII).
The Roman numeral for 1666, "MDCLXVI", uses each letter in descending order.


Symbol    Value
I          1
V          5
X          10
L          50
C          100
D          500
M          1,000
"""


def convert_to_int(roman_number):
    dict_values = {
        'I': 1,
        'V': 5,
        'X': 10,
        'L': 50,
        'C': 100,
        'D': 500,
        'M': 1000,
    }

    # Index -1 means last char
    print(roman_number[-1])

    numeric_number = 0

    # Will be needed to look next number
    roman_number_current_index = 0
    for i in roman_number:

        try:
            # If this letter is below next letter value, it means it's a substract
            if dict_values[i] < dict_values[roman_number[roman_number_current_index+1]]:
                numeric_number -= dict_values[i]
            else:
                numeric_number += dict_values[i]

            roman_number_current_index += 1
        except IndexError:
            numeric_number += dict_values[i]

    return numeric_number
