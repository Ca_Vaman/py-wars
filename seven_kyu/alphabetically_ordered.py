"""
https://www.codewars.com/kata/5a8059b1fd577709860000f6

Your task is very simple. Just write a function takes an input string of lowercase letters and returns true/false
depending on whether the string is in alphabetical order or not.
"""


def alphabetic(s):
    return ''.join(sorted(s)) == s
