"""
https://www.codewars.com/kata/58249d08b81f70a2fc0001a4

Given a number return the closest number to it that is divisible by 10.
"""


def closest_multiple_10(i):
    """
    Solution 1 : Basic one using integer division and remainder
    """
    #
    # # Here we'll get i = multiplier * 10 + remainder
    # multiplier = i // 10
    # remainder = i % 10
    #
    # # We get to know if remainder < 5 or not to get the closest
    # if remainder < 5:
    #     return multiplier * 10
    # else:
    #     return multiplier * 10 + 10

    """
    Solution 2 : using the power of round
    """
    # We divide by 10 and round at no decimal, then return result * 10
    return round(i/10, 0) * 10


