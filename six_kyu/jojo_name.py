"""
you are a JoJo if both your name and surname start with "Jo-".
One more complication comes with the third JoJo:
Jotaro Kujo (or, following Japanese use of putting the family name first: KuJo Jotaro).
One last variant comes with the fifth JoJo: as it is of Italian descent (well, sorta of)
and the traditional Italian alphabet has only 21 letters (lacking J, K, W, Y and X),
the name that you would pronounce Jorno Jovanna has to be spelled "Giorno Giovanna".

https://www.codewars.com/kata/55327e12f5363713200000e4

You are a Jojo if:
both your firstname and your surname start with "Jo-"
your firstname starts with "Jo-" and your surname ends with "-Jo"
both your firstname and your surname start with "Gio-"
don't expect the to have a string formed by only two words joined by a space: strings may be of 1 word, 2 words, 3 words or more
"""

# you can either create a valid regex to find if a character
# is a JoJo or create a function for it.
# bonus points if you do both ;)

# Oh, and think about beginners: try to be informative and
# comment with some explanation for the sake of their learning!

import re
regex = r"^(jo|gio)[a-z]* (jo|gio|[a-z]*jo$)"


def is_jojo(name):
    lowercase_name = name.lower()

    if re.match(regex, lowercase_name):
        return True

    return False
