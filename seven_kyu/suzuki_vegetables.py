"""
https://www.codewars.com/kata/56ff1667cc08cacf4b00171b/train/python

You will be given a string with the following vegetables:

"cabbage", "carrot", "celery", "cucumber", "mushroom", "onion", "pepper", "potato", "tofu", "turnip"

Return a list of tuples with the count of each vegetable in descending order.
"""


def count_vegetables(items_string):
    # We have to initialize the dict to use it after
    dict_result = dict()

    # We define the list of vegetables to remove non-vegetables (not tested but required in explanation text)
    vegetables = "cabbage", "carrot", "celery", "cucumber", "mushroom", "onion", "pepper", "potato", "tofu", "turnip"

    # We transform the string into a list using whitespace as a separator and sort descending as asked
    items_list = items_string.split(' ')

    for item in items_list:
        if item in vegetables:
            if item not in dict_result:
                dict_result[item] = 1
            else:
                dict_result[item] += 1

    # Turn dict into list of tuples in correct order and return it
    final_list = list()
    for item in dict_result:
        final_list.append((dict_result[item], item))

    final_list = sorted(final_list, reverse=True)
    return final_list
